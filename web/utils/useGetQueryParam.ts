import { useRouter } from "next/router";

export const useGetQueryParam = (param: string) => {
  const router = useRouter();
  return router.query[param];
};
