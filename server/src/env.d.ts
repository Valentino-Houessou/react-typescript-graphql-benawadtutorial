declare namespace NodeJS {
  export interface ProcessEnv {
    NODE_ENV: string;
    DB_NAME: string;
    DB_USER: string;
    DB_PASSWORD: string;
    REDIS_PASSWORD: string;
    ETHEREAL_USER: string;
    ETHEREAL_PASSWORD: string;
    WEB_URL: string;
    PORT: string;
    REDIS_URL: string;
    SESSION_SECRET: string;
  }
}
