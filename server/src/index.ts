import { ApolloServer } from "apollo-server-express";
import connectRedis from "connect-redis";
import cors from "cors";
import express from "express";
import session from "express-session";
import Redis from "ioredis";
import path from "path";
import "reflect-metadata";
import { buildSchema } from "type-graphql";
import { createConnection } from "typeorm";
import {
  COOKIE_NAME,
  __db_password__,
  __db_user__,
  __prod__,
  __redis_password__,
  __web_url__,
  __port__,
  __db_name__,
  __redis_url__,
  __session_secret__,
} from "./constants";
import { Post } from "./entities/Posts";
import { Updoot } from "./entities/Updoot";
import { User } from "./entities/User";
import { HelloResolver } from "./resolvers/hello";
import { PostResolver } from "./resolvers/post";
import { UserResolver } from "./resolvers/user";
import { createUpdootLoader } from "./utils/createUpdootLoader";
import { createUserLoader } from "./utils/createUserLoader";

const main = async () => {
  // create a connection to the database
  const conn = await createConnection({
    type: "postgres",
    database: __db_name__,
    username: __db_user__,
    password: __db_password__,
    logging: true,
    // synchronize: true, // automatically create the tables without need of migration
    migrations: [path.join(__dirname, "./migrations/*")],
    entities: [Post, User, Updoot],
  });

  await conn.runMigrations();

  // await Post.delete({});

  const app = express();

  //set up redis and store session object in redis store
  const RedisStore = connectRedis(session);
  const redis = new Redis(__redis_url__, {
    password: __redis_password__ || "",
  });

  app.set("proxy", 1);

  //set up cors with express cors middleware
  app.use(cors({ origin: __web_url__, credentials: true }));
  // set up session middleware
  app.use(
    session({
      name: COOKIE_NAME,
      store: new RedisStore({
        client: redis,
        disableTouch: true,
      }),
      cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 365 * 10, // 10 years
        httpOnly: true,
        sameSite: "lax", // csrf ( cross-site request forgery)
        secure: __prod__, // cookie only works in https
        domain: __prod__ ? ".codeponder.com" : undefined,
      },
      saveUninitialized: false,
      secret: __session_secret__,
      resave: false,
    })
  );

  // set up apollo middleware
  // context is a special object that is accessible by all the resolvers
  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: [HelloResolver, PostResolver, UserResolver],
      validate: false,
    }),
    context: ({ req, res }) => ({
      req,
      res,
      redis,
      userLoader: createUserLoader(),
      updootLoader: createUpdootLoader(),
    }),
  });

  // create a /graphql endpoint on express
  apolloServer.applyMiddleware({
    app,
    cors: false,
  });
  // the backend will listen to all request on configured port
  app.listen(parseInt(__port__), () => {
    console.log(`server started on port ${__port__}`);
  });
};

main().catch((err) => {
  console.log(err);
});
